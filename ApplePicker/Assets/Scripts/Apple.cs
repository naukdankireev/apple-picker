using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Apple : MonoBehaviour
{
    public float bottomY = -15f;
    void Awake()
    {
        
    }
    
    void Update()
    {
        if (transform.position.y < bottomY)
        {
            Destroy(this.gameObject);
            Game allAppleDestroy = GameObject.Find("Game").GetComponent<Game>();
            allAppleDestroy.AppleDestroyed();
        }
    }
}
