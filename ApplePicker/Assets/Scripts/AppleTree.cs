using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AppleTree : MonoBehaviour
{
    [SerializeField]
    private GameObject _applePrefab;

    [SerializeField]
    private GameObject _bombPrefab;

    [SerializeField]
    private float secondsBetweenAppleDrops = 1f;

    public float speed = 10f;
    public float leftAndRightEdge = 31f;
    public float chanceToChangeDirection = 0.01f;

    void Start()
    {
        DropApple();
        DropBomb();
    }

    void Update()
    {
        MovingAppleTree();
        ChangeDirection();
    }

    void FixedUpdate()
    {
        RandomChangeDirection();
    }

    void MovingAppleTree()
    {
        Vector3 pos = transform.position;
        pos.x += speed * Time.deltaTime;
        transform.position = pos;
    }

    void ChangeDirection()
    {
        Vector3 pos = transform.position;
        if (pos.x < -leftAndRightEdge)
        {
            speed = Mathf.Abs(speed);
        }
        else if (pos.x > leftAndRightEdge)
        {
            speed = -Mathf.Abs(speed);
        }
    }

    void RandomChangeDirection()
    {

        if (Random.value < chanceToChangeDirection)
        {
            speed *= -1;
        }
    }

    void DropApple()
    {
        GameObject apple = Instantiate<GameObject>(_applePrefab);
        apple.transform.position = transform.position;
        Invoke("DropApple", secondsBetweenAppleDrops);
    }

    void DropBomb()
    {
        GameObject bomb = Instantiate <GameObject> (_bombPrefab);
        bomb.transform.position = transform.position;
        Invoke("DropBomb", secondsBetweenAppleDrops);
    }
}