using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    [SerializeField]
    private GameObject _basketPrefab;
    [SerializeField]
    private int _numBasket = 3;
    [SerializeField]
    private float _basketBottomY = -10f;
    [SerializeField]
    private float _basketSpacingY = 2f;
    
    [SerializeField]
    private int _pointsToLevelUp = 1000;

    private int _levelUpPoints = 1000;

    [SerializeField]
    private float _difficultyFactor = 1.5f;
    
    public bool _isLevelComplited = false;

    public List<GameObject> basketList;

    private int _highScore = 0;

    private AppleTree appleTree;
    
    public float mass;
    public Rigidbody rb;

    private void Awake()
    {
        LoadScore();
    }

    void Start()
    {
        appleTree = GameObject.Find("AppleTree").GetComponent<AppleTree>();
        
        basketList = new List<GameObject>();
        for (int i = 0; i < _numBasket; i++)
        {
            GameObject tbsaketGo = Instantiate<GameObject>(_basketPrefab);
            Vector3 pos = Vector3.zero;
            pos.y = _basketBottomY + (_basketSpacingY * i);
            tbsaketGo.transform.position = pos;
            basketList.Add(tbsaketGo);
        }
        
        SaveScore();
    }
    
    public void AppleDestroyed()
    {
        GameObject[] tAppleArray = GameObject.FindGameObjectsWithTag("Apple");
        foreach (GameObject item in tAppleArray)
        {
            Destroy(item);
        }

        int basketIndex = basketList.Count - 1;
        GameObject tbasketGo = basketList[basketIndex];
        basketList.RemoveAt(basketIndex);
        Destroy(tbasketGo);

        if (basketList.Count == 0)
        {
            SaveScore();
            SceneManager.LoadScene("SampleScene");
        }
    }
    
    public void SaveScore()
    {
        GameObject scoreGO = GameObject.Find("ScoreCounter");
        TMP_Text scoreText = scoreGO.GetComponent<TMP_Text>();
        if (_highScore < int.Parse(scoreText.text))
        {
            _highScore = int.Parse((scoreText.text));
            PlayerPrefs.SetInt("HighScore", _highScore);
        }
    }

    private void LoadScore()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            _highScore = PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            _highScore = 0;
        }

        GameObject scoreGO = GameObject.Find("HighScore");
        TMP_Text scoreText = scoreGO.GetComponent<TMP_Text>();
        scoreText.text = "High Score: " + _highScore;

        scoreText = GameObject.Find("ScoreCounter").GetComponent<TMP_Text>();
        scoreText.text = "0";
    }
    
    public void ScoreIncrease()
    {
        GameObject scoreGO = GameObject.Find("ScoreCounter");
        TMP_Text scoreText = scoreGO.GetComponent<TMP_Text>();
        int sc = int.Parse(scoreText.text);
        sc += 100;
        scoreText.text = sc.ToString();
        
        //Усложнение игры
        if (sc >= _pointsToLevelUp)
        {
            appleTree.speed *= _difficultyFactor;
            appleTree.chanceToChangeDirection *= _difficultyFactor;
            _pointsToLevelUp += _levelUpPoints;
        }
    }
}