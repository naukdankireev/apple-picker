using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void StartGame()
    {
       PlayerPrefs.DeleteAll();
       SceneManager.LoadScene("SampleScene");
    }

    public void CrazyLevel()
    {
        SceneManager.LoadScene("CrazyLevel");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    
    public void BackToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
